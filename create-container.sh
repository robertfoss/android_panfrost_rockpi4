#!/bin/bash

set -e
set -o xtrace

export DEBIAN_FRONTEND=noninteractive

apt-get install -y ca-certificates

echo 'deb http://ftp.debian.org/debian unstable main' > /etc/apt/sources.list.d/unstable.list

apt-get update

apt-get dist-upgrade -y

apt-get install -y --no-remove \
                git-core \
                gnupg \
                flex \
                bison \
                gperf \
                build-essential \
                zip \
                curl \
                zlib1g-dev \
                gcc-multilib \
                g++-multilib \
                libc6-dev-i386 \
                lib32ncurses5-dev \
                x11proto-core-dev \
                libx11-dev \
                lib32z-dev \
                libgl1-mesa-dev \
                libxml2-utils \
                xsltproc \
                unzip \
                python \
                libncurses5 \
                ccache

mkdir -p /usr/share/man/man1
apt-get install -y -t unstable openjdk-8-jdk

curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
chmod a+x /usr/local/bin/repo

git config --global user.name 'Android Build'
git config --global user.email 'foo@example.com'
git config --global color.ui true

mkdir -p /ccache
export USE_CCACHE=1
export CCACHE_DIR=/ccache

mkdir -p /aosp
pushd /aosp
repo init --depth=1 -u https://android.googlesource.com/platform/manifest -b ${CI_COMMIT_REF_NAME}

pushd .repo
git clone https://gitlab.collabora.com/android_panfrost/android_manifest.git
popd

repo sync -q -f --force-sync --no-clone-bundle --no-tags -j10
du -sh /aosp

export USER=$(whoami)
source build/envsetup.sh
lunch aosp_arm64-eng
#m > /dev/null
du -sh /aosp

popd

